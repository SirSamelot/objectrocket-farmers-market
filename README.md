# ObjectRocket Dev Test - Farmer's Market

Implement a simple checkout system based on the requirements [here](https://gist.github.com/keithhigbee/0473b604b067a0b945ceea845dde419e).


## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.  **The application is running on Django's development server with DEBUG=True.**  See [here](https://docs.djangoproject.com/en/2.0/ref/settings/#debug) for further information and why this is **NOT** appropriate for production environments.


### Prerequisites:

* Docker Toolbox (see [footnote](#markdown-header-troubleshooting-the-docker-image)) installed

### Installing

1. Download or clone the repo to a folder on your local machine
2. Start Docker Quickstart Terminal
- All subsequent commands must be run from this terminal.
3. Navigate to the folder containing the repo in step 1.
4. Navigate to the folder containing the Dockerfile
5. Note down the docker machine's IP address with the following command:
   ```docker-machine ip```
6. Create the docker image with the following command:
   ```docker build -t <name> .```

    - Replace ```<name>``` with a name for the docker image
    - Don't forget the ```.``` at the end of the command

7. Create and run a container of the image:
   ```docker run -it -p 80:8000 <name>```

    - Replace ```<name>``` with the name of the docker image created above
    - The Docker Terminal should indicate that the Django development server is running.

8. Congratulations!  You have finished the installation.

## Running the App
Open up a browser and enter the docker machine's IP address in the address bar

- Although the development server says it is running on 0.0.0.0:8000, you must enter the docker machine's IP address
- You should see "Welcome to the ObjectRocket Farmer's Market!" in your browser

## Using the App
- Click on the Checkout link in the welcome page to access the Checkout system
- Enter product codes and click ```Add Product Code``` to add them to the basket
- Click ```Print Transaction``` to display the current state of the basket
- Click ```New Transaction``` to empty the current basket


## Running the Tests

The app uses Python's unittest framework for automated testing and the ```coverage``` library for code coverage.  To run the tests, follow these steps:

1. In a terminal window, navigate to the folder containing manage.py
2. Run the following command:
   ```coverage run manage.py test```
3. To view the coverage report:
   ```coverage report```


## Built With

* [Python](https://www.python.org/)
* [Django](https://www.djangoproject.com/)
* [Docker Toolbox](https://docs.docker.com/toolbox/overview/)


## Troubleshooting the Docker Image
* I **really** hope the Dockerfile builds and runs correctly on your system.  I was only able to develop locally on Windows7.  It's also the reason I had to use Docker Toolbox and not normal Docker.  I have never used Docker until yesterday, but I managed to get it working with the [installation procedure](#markdown-header-installing) documented above.

* If the Dockerfile doesn't work, the following steps can be done.

1. Create a virtual env (python -m venv env) where you cloned the repo
2. Activate the virtual env
3. Navigate to the farmers_market folder where requirements.txt is located
4. Install the requirements (pip install -r requirements.txt)
5. Run the development server (python manage.py runserver)
6. Open 127.0.0.1:8000 in your browser.  If port 8000 doesn't work, you can try running the development server on another port e.g. python manage.py runserver localhost:8080


## Authors

* **Sam Wong**
