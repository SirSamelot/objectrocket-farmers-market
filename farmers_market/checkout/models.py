from django.db import models
from django.core.validators import RegexValidator, MinValueValidator


class Product(models.Model):
    """Product available in Farmer's Market."""
    code = models.CharField(
        'Product Code',
        help_text='e.g. CF1',
        max_length=16,
        unique=True,
        validators=[RegexValidator(
            regex=r'^[A-Z][A-Z][0-9]$',
            message='Product Code must be two uppercase letters followed by a single digit, e.g. CF1')])
    name = models.CharField(max_length=200)
    price = models.DecimalField(
        max_digits=8,
        decimal_places=2,
        validators=[MinValueValidator(0)])

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['pk']
