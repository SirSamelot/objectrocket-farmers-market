"""Custom Django template tags for Checkout app."""

from django import template

register = template.Library()


@register.inclusion_tag('checkout/tag_transaction_record.html', takes_context=True)
def transaction_record(context):
    """Renders the Transaction Record."""
    request = context['request']
    tx_record = request.session.get('tx_record')
    if tx_record:
        return {'tx_record': tx_record}
    else:
        return None
