from django.test import TestCase, RequestFactory
from django.contrib.sessions.middleware import SessionMiddleware
from django.contrib.messages.middleware import MessageMiddleware
from django.urls import reverse

from .models import Product
from . import views


class ProductModelTests(TestCase):
    """Tests for Product model."""
    @classmethod
    def setUpTestData(cls):
        """Set up data for TestCase."""
        Product.objects.create(code='CF1', name='Coffee', price=11.23)

    def test_code_label(self):
        """Fails if code label is unexpected."""
        product = Product.objects.get(id=1)
        field_label = product._meta.get_field('code').verbose_name
        self.assertEquals(field_label, 'Product Code')

    def test_code_max_length(self):
        """Fails if code label max_length is unexpected."""
        product = Product.objects.get(id=1)
        max_length = product._meta.get_field('code').max_length
        self.assertEquals(max_length, 16)

    def test_name_label(self):
        """Fails if name label is unexpected."""
        product = Product.objects.get(id=1)
        field_label = product._meta.get_field('name').verbose_name
        self.assertEquals(field_label, 'name')

    def test_name_max_length(self):
        """Fails if name label max_length is unexpected."""
        product = Product.objects.get(id=1)
        max_length = product._meta.get_field('name').max_length
        self.assertEquals(max_length, 200)

    def test_price_label(self):
        """Fails if price label is unexpected."""
        product = Product.objects.get(id=1)
        field_label = product._meta.get_field('price').verbose_name
        self.assertEquals(field_label, 'price')

    def test_price_max_digits(self):
        """Fails if price max_digits is unexpected."""
        product = Product.objects.get(id=1)
        max_digits = product._meta.get_field('price').max_digits
        self.assertEquals(max_digits, 8)

    def test_price_decimal_places(self):
        """Fails if price decimal places is unexpected."""
        product = Product.objects.get(id=1)
        decimal_places = product._meta.get_field('price').decimal_places
        self.assertEquals(decimal_places, 2)

    def test_object_name_is_name(self):
        product = Product.objects.get(id=1)
        expected_object_name = product.name
        self.assertEquals(expected_object_name, str(product))


class CheckoutViewTests(TestCase):
    """Tests for CheckoutView."""
    @classmethod
    def setUpTestData(cls):
        """Set up data for TestCase."""
        Product.objects.create(code='CH1', name='Chai', price=3.11)
        Product.objects.create(code='AP1', name='Apples', price=6.00)
        Product.objects.create(code='CF1', name='Coffee', price=11.23)
        Product.objects.create(code='MK1', name='Milk', price=4.75)
        Product.objects.create(code='OM1', name='Oatmeal', price=3.69)

    def setup_middleware(self, request):
        """Set up middleware for tests requiring session and messages."""
        # Add session middleware to request object
        middleware = SessionMiddleware()
        middleware.process_request(request)
        request.session.save()

        # Add messages middleware to request object
        middleware = MessageMiddleware()
        middleware.process_request(request)
        request.session.save()

    def test_checkout_view_get(self):
        """Fails if checkout view does not respond to GET requests."""
        request = RequestFactory().get(reverse('checkout:index'))
        response = views.CheckoutView.as_view()(request)
        self.assertEqual(response.status_code, 200)

    def test_checkout_view_post_add_valid_product_code(self):
        """Fails if checkout view does not respond to POST requests."""
        valid_code = 'CF1'
        data = {'add_product_code': True, 'product_code': valid_code}
        request = RequestFactory().post(reverse('checkout:index'), data)
        self.setup_middleware(request)

        response = views.CheckoutView.as_view()(request)

        self.assertEqual(response.status_code, 302)
        self.assertEqual(request.session.get('tx'), [valid_code])
        self.assertEqual(request._messages._queued_messages[0].message, \
                         valid_code + ' successfully added to transaction')

    def test_checkout_view_post_add_invalid_product_code(self):
        """Fails if checkout view does not respond to POST requests."""
        invalid_code = ' '  # a space is considered an invalid product code
        data = {'add_product_code': True, 'product_code': invalid_code}
        request = RequestFactory().post(reverse('checkout:index'), data)
        self.setup_middleware(request)

        response = views.CheckoutView.as_view()(request)

        self.assertEqual(response.status_code, 302)
        self.assertIsNone(request.session.get('tx'))
        self.assertEqual(request._messages._queued_messages[0].message, \
                         'Invalid product code ' + invalid_code + ' submitted. Please try again.')

    def test_checkout_view_post_new_transaction(self):
        """Fails if checkout view does not respond to POST requests."""
        data = {'new_transaction': True}
        request = RequestFactory().post(reverse('checkout:index'), data)
        self.setup_middleware(request)

        response = views.CheckoutView.as_view()(request)

        self.assertEqual(response.status_code, 302)
        self.assertIsNone(request.session.get('tx'))
        self.assertEqual(request._messages._queued_messages[0].message, \
                         'Starting a new transaction')

    def test_checkout_view_print_transaction_empty_basket(self):
        """Fails if checkout view does not respond to POST requests."""
        data = {'print_transaction': True}
        request = RequestFactory().post(reverse('checkout:index'), data)
        self.setup_middleware(request)

        response = views.CheckoutView.as_view()(request)

        self.assertEqual(response.status_code, 200)
        self.assertIsNone(request.session.get('tx_record'))

    def test_checkout_view_print_transaction_with_basket(self):
        """Fails if checkout view does not respond to POST requests."""
        data = {'print_transaction': True}
        request = RequestFactory().post(reverse('checkout:index'), data)

        self.setup_middleware(request)

        # add item to basket
        request.session['tx'] = ['CF1']

        response = views.CheckoutView.as_view()(request)

        self.assertEqual(response.status_code, 200)
        self.assertIsNotNone(request.session.get('tx_record'))


class CheckoutViewHelperFunctionsTests(TestCase):
    """Tests for the Checkout View Helper Functions."""
    @classmethod
    def setUpTestData(cls):
        """Set up data for the entire TestCase."""
        Product.objects.create(code='CH1', name='Chai', price=3.11)
        Product.objects.create(code='AP1', name='Apples', price=6.00)
        Product.objects.create(code='CF1', name='Coffee', price=11.23)
        Product.objects.create(code='MK1', name='Milk', price=4.75)
        Product.objects.create(code='OM1', name='Oatmeal', price=3.69)

    def setUp(self):
        """Set up data that needs to be reset between tests."""
        # create Request object and add session
        self.request = RequestFactory().get(reverse('checkout:index'))
        self.middleware = SessionMiddleware()
        self.middleware.process_request(self.request)
        self.request.session.save()

    # Tests for validate_product_code()
    def test_is_valid_product_code_uppercase_found(self):
        """Fails if existing product code in uppercase isn't found."""
        self.assertTrue(views.validate_product_code('CH1'))

    def test_is_valid_product_code_lowercase_found(self):
        """Fails if existing product code in lowercase isn't found."""
        self.assertTrue(views.validate_product_code('ch1'))

    def test_is_valid_product_code_mixed_case_found(self):
        """Fails if existing product code in mixed case isn't found."""
        self.assertTrue(views.validate_product_code('cH1'))

    def test_is_invalid_product_code_not_found(self):
        """Fails if product code which doesn't exist is found."""
        self.assertFalse(views.validate_product_code('abc'))

    def test_is_blank_product_code_not_found(self):
        """Fails if blank product code is found."""
        self.assertFalse(views.validate_product_code(''))

    # Tests for add_product_to_transaction()
    def test_string_added_to_new_session(self):
        """Fails if string isn't added to new session."""
        self.assertIsNone(self.request.session.get('tx'))

        views.add_product_to_transaction(self.request, 'CH1')
        self.assertIsNotNone(self.request.session.get('tx'))
        self.assertEqual(self.request.session['tx'], ['CH1'])

    def test_string_added_to_existing_session(self):
        """Fails if string isn't added to existing session."""
        self.request.session['tx'] = ['CF1']
        self.assertEqual(self.request.session['tx'], ['CF1'])

        views.add_product_to_transaction(self.request, 'CH1')
        self.assertEqual(self.request.session['tx'], ['CF1', 'CH1'])

    # Tests for get_tx_record()
    def test_get_tx_record(self):
        """STUB when get_tx_record() is further implemented.

        See tests for apply_discounts()"""
        pass

    # Tests for apply_discounts()
    def test_tx_record_empty_basket(self):
        """Fails if tx_record is created with an empty basket."""
        basket = []
        tx_record = views.apply_discounts(basket)
        self.assertIsNone(tx_record)

    def test_no_discounts_one_item_(self):
        """Fails if incorrect tx_record with 1 item, no discounts."""
        basket = ['CF1']
        tx_record = views.apply_discounts(basket)
        expected = [
            {'line_num': 0, 'is_discount': False, 'code': 'CF1', 'amount': '11.23'},
            {'line_num': 1, 'total': '11.23'}
        ]
        self.assertEqual(tx_record, expected)

    def test_no_discounts_two_item(self):
        """Fails if incorrect output with two items, no discounts."""
        basket = ['CF1', 'AP1']
        tx_record = views.apply_discounts(basket)
        expected = [
            {'line_num': 0, 'is_discount': False, 'code': 'CF1', 'amount': '11.23'},
            {'line_num': 1, 'is_discount': False, 'code': 'AP1', 'amount': '6.00'},
            {'line_num': 2, 'total': '17.23'}
        ]
        self.assertEqual(tx_record, expected)

    def test_no_discounts_multiple_item(self):
        """Fails if incorrect output with multiple items, no discounts."""
        basket = ['CF1', 'AP1', 'AP1', 'MK1']
        tx_record = views.apply_discounts(basket)
        expected = [
            {'line_num': 0, 'is_discount': False, 'code': 'CF1', 'amount': '11.23'},
            {'line_num': 1, 'is_discount': False, 'code': 'AP1', 'amount': '6.00'},
            {'line_num': 2, 'is_discount': False, 'code': 'AP1', 'amount': '6.00'},
            {'line_num': 3, 'is_discount': False, 'code': 'MK1', 'amount': '4.75'},
            {'line_num': 4, 'total': '27.98'}
        ]
        self.assertEqual(tx_record, expected)

    def test_BOGO_discount_does_not_apply(self):
        """Fails if BOGO applied when zero or one CF1 in basket."""
        basket_no_CF1 = ['AP1']
        tx_record_no_CF1 = views.apply_discounts(basket_no_CF1)
        expected_no_CF1 = [
            {'line_num': 0, 'is_discount': False, 'code': 'AP1', 'amount': '6.00'},
            {'line_num': 1, 'total': '6.00'}
        ]
        self.assertEqual(tx_record_no_CF1, expected_no_CF1)

        basket_one_CF1 = ['CF1', 'AP1']
        tx_record_one_CF1 = views.apply_discounts(basket_one_CF1)
        expected_one_CF1 = [
            {'line_num': 0, 'is_discount': False, 'code': 'CF1', 'amount': '11.23'},
            {'line_num': 1, 'is_discount': False, 'code': 'AP1', 'amount': '6.00'},
            {'line_num': 2, 'total': '17.23'}
        ]
        self.assertEqual(tx_record_one_CF1, expected_one_CF1)

    def test_BOGO_one_discount(self):
        """Fails if one and only one BOGO isn't applied with two CF1 in basket."""
        basket = ['CF1', 'AP1', 'CF1']
        tx_record = views.apply_discounts(basket)
        expected = [
            {'line_num': 0, 'is_discount': False, 'code': 'CF1', 'amount': '11.23'},
            {'line_num': 1, 'is_discount': False, 'code': 'AP1', 'amount': '6.00'},
            {'line_num': 2, 'is_discount': False, 'code': 'CF1', 'amount': '11.23'},
            {'line_num': 3, 'is_discount': True, 'code': 'BOGO', 'amount': '-11.23'},
            {'line_num': 4, 'total': '17.23'}
        ]
        self.assertEqual(tx_record, expected)

    def test_BOGO_correct_number_applied(self):
        """Fails if number of BOGO applied is not equal to (number of CF1) // 2."""
        basket_3_CF1 = ['CF1', 'AP1', 'CF1', 'CF1']
        tx_record_3_CF1 = views.apply_discounts(basket_3_CF1)
        expected_1_BOGO = [
            {'line_num': 0, 'is_discount': False, 'code': 'CF1', 'amount': '11.23'},
            {'line_num': 1, 'is_discount': False, 'code': 'AP1', 'amount': '6.00'},
            {'line_num': 2, 'is_discount': False, 'code': 'CF1', 'amount': '11.23'},
            {'line_num': 3, 'is_discount': True, 'code': 'BOGO', 'amount': '-11.23'},
            {'line_num': 4, 'is_discount': False, 'code': 'CF1', 'amount': '11.23'},
            {'line_num': 5, 'total': '28.46'}
        ]
        self.assertEqual(tx_record_3_CF1, expected_1_BOGO)

        basket_4_CF1 = ['CF1', 'AP1', 'CF1', 'CF1', 'CF1']
        tx_record_4_CF1 = views.apply_discounts(basket_4_CF1)
        expected_2_BOGO = [
            {'line_num': 0, 'is_discount': False, 'code': 'CF1', 'amount': '11.23'},
            {'line_num': 1, 'is_discount': False, 'code': 'AP1', 'amount': '6.00'},
            {'line_num': 2, 'is_discount': False, 'code': 'CF1', 'amount': '11.23'},
            {'line_num': 3, 'is_discount': True, 'code': 'BOGO', 'amount': '-11.23'},
            {'line_num': 4, 'is_discount': False, 'code': 'CF1', 'amount': '11.23'},
            {'line_num': 5, 'is_discount': False, 'code': 'CF1', 'amount': '11.23'},
            {'line_num': 6, 'is_discount': True, 'code': 'BOGO', 'amount': '-11.23'},
            {'line_num': 7, 'total': '28.46'}
        ]
        self.assertEqual(tx_record_4_CF1, expected_2_BOGO)

    def test_CHMK_does_not_apply_with_CH1_only(self):
        """Fails if CHMK is applied when only CH1 in basket."""
        basket = ['CH1', 'AP1', 'CF1']
        tx_record = views.apply_discounts(basket)
        expected = [
            {'line_num': 0, 'is_discount': False, 'code': 'CH1', 'amount': '3.11'},
            {'line_num': 1, 'is_discount': False, 'code': 'AP1', 'amount': '6.00'},
            {'line_num': 2, 'is_discount': False, 'code': 'CF1', 'amount': '11.23'},
            {'line_num': 3, 'total': '20.34'}
        ]
        self.assertEqual(tx_record, expected)

    def test_CHMK_does_not_apply_with_MK1_only(self):
        """Fails if CHMK is applied when only MK1 in basket."""
        basket = ['MK1', 'AP1', 'CF1']
        tx_record = views.apply_discounts(basket)
        expected = [
            {'line_num': 0, 'is_discount': False, 'code': 'MK1', 'amount': '4.75'},
            {'line_num': 1, 'is_discount': False, 'code': 'AP1', 'amount': '6.00'},
            {'line_num': 2, 'is_discount': False, 'code': 'CF1', 'amount': '11.23'},
            {'line_num': 3, 'total': '21.98'}
        ]
        self.assertEqual(tx_record, expected)

    def test_CHMK_applied_with_one_each_in_basket(self):
        """Fails if CHMK is not applied when one each CH1 and MK1 in basket."""
        basket = ['MK1', 'CH1', 'AP1', 'CF1']
        tx_record = views.apply_discounts(basket)
        expected = [
            {'line_num': 0, 'is_discount': False, 'code': 'MK1', 'amount': '4.75'},
            {'line_num': 1, 'is_discount': True, 'code': 'CHMK', 'amount': '-4.75'},
            {'line_num': 2, 'is_discount': False, 'code': 'CH1', 'amount': '3.11'},
            {'line_num': 3, 'is_discount': False, 'code': 'AP1', 'amount': '6.00'},
            {'line_num': 4, 'is_discount': False, 'code': 'CF1', 'amount': '11.23'},
            {'line_num': 5, 'total': '20.34'}
        ]
        self.assertEqual(tx_record, expected)

    def test_CHMK_correct_number_applied(self):
        """Fails if number of CHMK applied is > 1 when both MK1 and CH1 > 1."""
        basket = ['CH1', 'MK1', 'MK1', 'CH1', 'MK1']
        tx_record = views.apply_discounts(basket)
        expected = [
            {'line_num': 0, 'is_discount': False, 'code': 'CH1', 'amount': '3.11'},
            {'line_num': 1, 'is_discount': False, 'code': 'MK1', 'amount': '4.75'},
            {'line_num': 2, 'is_discount': True, 'code': 'CHMK', 'amount': '-4.75'},
            {'line_num': 3, 'is_discount': False, 'code': 'MK1', 'amount': '4.75'},
            {'line_num': 4, 'is_discount': False, 'code': 'CH1', 'amount': '3.11'},
            {'line_num': 5, 'is_discount': False, 'code': 'MK1', 'amount': '4.75'},
            {'line_num': 6, 'total': '15.72'}
        ]
        self.assertEqual(tx_record, expected)

    def test_APPL_only(self):
        """Fails if APPL not applied when AP1 quantity > 3."""
        basket = ['CH1', 'AP1', 'AP1', 'AP1', 'CF1', 'AP1']
        tx_record = views.apply_discounts(basket)
        expected = [
            {'line_num': 0, 'is_discount': False, 'code': 'CH1', 'amount': '3.11'},
            {'line_num': 1, 'is_discount': False, 'code': 'AP1', 'amount': '6.00'},
            {'line_num': 2, 'is_discount': True, 'code': 'APPL', 'amount': '-1.50'},
            {'line_num': 3, 'is_discount': False, 'code': 'AP1', 'amount': '6.00'},
            {'line_num': 4, 'is_discount': True, 'code': 'APPL', 'amount': '-1.50'},
            {'line_num': 5, 'is_discount': False, 'code': 'AP1', 'amount': '6.00'},
            {'line_num': 6, 'is_discount': True, 'code': 'APPL', 'amount': '-1.50'},
            {'line_num': 7, 'is_discount': False, 'code': 'CF1', 'amount': '11.23'},
            {'line_num': 8, 'is_discount': False, 'code': 'AP1', 'amount': '6.00'},
            {'line_num': 9, 'is_discount': True, 'code': 'APPL', 'amount': '-1.50'},
            {'line_num': 10, 'total': '32.34'}
        ]
        self.assertEqual(tx_record, expected)

    def test_APPL_only_not_enough_AP1_to_qualify(self):
        """Fails if APPL applied when AP1 quantity < 3."""
        basket = ['CH1', 'AP1', 'AP1']
        tx_record = views.apply_discounts(basket)
        expected = [
            {'line_num': 0, 'is_discount': False, 'code': 'CH1', 'amount': '3.11'},
            {'line_num': 1, 'is_discount': False, 'code': 'AP1', 'amount': '6.00'},
            {'line_num': 2, 'is_discount': False, 'code': 'AP1', 'amount': '6.00'},
            {'line_num': 3, 'total': '15.11'}
        ]
        self.assertEqual(tx_record, expected)

    def test_APOM_only_equal_quantity_of_each(self):
        """Fails if incorrect number of APOM applied when AP1 and OM1 in basket."""
        basket = ['CH1', 'AP1', 'AP1', 'OM1', 'OM1', 'CF1']
        tx_record = views.apply_discounts(basket)
        expected = [
            {'line_num': 0, 'is_discount': False, 'code': 'CH1', 'amount': '3.11'},
            {'line_num': 1, 'is_discount': False, 'code': 'AP1', 'amount': '6.00'},
            {'line_num': 2, 'is_discount': True, 'code': 'APOM', 'amount': '-3.00'},
            {'line_num': 3, 'is_discount': False, 'code': 'AP1', 'amount': '6.00'},
            {'line_num': 4, 'is_discount': True, 'code': 'APOM', 'amount': '-3.00'},
            {'line_num': 5, 'is_discount': False, 'code': 'OM1', 'amount': '3.69'},
            {'line_num': 6, 'is_discount': False, 'code': 'OM1', 'amount': '3.69'},
            {'line_num': 7, 'is_discount': False, 'code': 'CF1', 'amount': '11.23'},
            {'line_num': 8, 'total': '27.72'}
        ]
        self.assertEqual(tx_record, expected)

    def test_APOM_only_num_AP1_greater_than_num_OM1(self):
        """Fails if incorrect number of APOM applied when AP1 and OM1 in basket."""
        basket = ['CH1', 'AP1', 'AP1', 'OM1', 'CF1']
        tx_record = views.apply_discounts(basket)
        expected = [
            {'line_num': 0, 'is_discount': False, 'code': 'CH1', 'amount': '3.11'},
            {'line_num': 1, 'is_discount': False, 'code': 'AP1', 'amount': '6.00'},
            {'line_num': 2, 'is_discount': True, 'code': 'APOM', 'amount': '-3.00'},
            {'line_num': 3, 'is_discount': False, 'code': 'AP1', 'amount': '6.00'},
            {'line_num': 4, 'is_discount': False, 'code': 'OM1', 'amount': '3.69'},
            {'line_num': 5, 'is_discount': False, 'code': 'CF1', 'amount': '11.23'},
            {'line_num': 6, 'total': '27.03'}
        ]
        self.assertEqual(tx_record, expected)

    def test_APOM_only_num_AP1_less_than_num_OM1(self):
        """Fails if incorrect number of APOM applied when AP1 and OM1 in basket."""
        basket = ['CH1', 'AP1', 'OM1', 'OM1', 'CF1']
        tx_record = views.apply_discounts(basket)
        expected = [
            {'line_num': 0, 'is_discount': False, 'code': 'CH1', 'amount': '3.11'},
            {'line_num': 1, 'is_discount': False, 'code': 'AP1', 'amount': '6.00'},
            {'line_num': 2, 'is_discount': True, 'code': 'APOM', 'amount': '-3.00'},
            {'line_num': 3, 'is_discount': False, 'code': 'OM1', 'amount': '3.69'},
            {'line_num': 4, 'is_discount': False, 'code': 'OM1', 'amount': '3.69'},
            {'line_num': 5, 'is_discount': False, 'code': 'CF1', 'amount': '11.23'},
            {'line_num': 6, 'total': '24.72'}
        ]
        self.assertEqual(tx_record, expected)

    def test_APPL_and_APOM_equal_quantity_of_each(self):
        """Fails if incorrect number of APPL and APOM when AP1 and OM1 in basket."""
        basket = ['OM1', 'OM1', 'OM1', 'OM1', 'AP1', 'AP1', 'AP1', 'AP1']
        tx_record = views.apply_discounts(basket)
        expected = [
            {'line_num': 0, 'is_discount': False, 'code': 'OM1', 'amount': '3.69'},
            {'line_num': 1, 'is_discount': False, 'code': 'OM1', 'amount': '3.69'},
            {'line_num': 2, 'is_discount': False, 'code': 'OM1', 'amount': '3.69'},
            {'line_num': 3, 'is_discount': False, 'code': 'OM1', 'amount': '3.69'},
            {'line_num': 4, 'is_discount': False, 'code': 'AP1', 'amount': '6.00'},
            {'line_num': 5, 'is_discount': True, 'code': 'APOM', 'amount': '-3.00'},
            {'line_num': 6, 'is_discount': False, 'code': 'AP1', 'amount': '6.00'},
            {'line_num': 7, 'is_discount': True, 'code': 'APOM', 'amount': '-3.00'},
            {'line_num': 8, 'is_discount': False, 'code': 'AP1', 'amount': '6.00'},
            {'line_num': 9, 'is_discount': True, 'code': 'APOM', 'amount': '-3.00'},
            {'line_num': 10, 'is_discount': False, 'code': 'AP1', 'amount': '6.00'},
            {'line_num': 11, 'is_discount': True, 'code': 'APOM', 'amount': '-3.00'},
            {'line_num': 12, 'total': '26.76'}
        ]
        self.assertEqual(tx_record, expected)

    def test_APPL_and_APOM_num_AP1_greater_than_num_OM1(self):
        """Fails if incorrect number of APPL and APOM when AP1 and OM1 in basket."""
        basket = ['OM1', 'OM1', 'AP1', 'AP1', 'AP1', 'AP1']
        tx_record = views.apply_discounts(basket)
        expected = [
            {'line_num': 0, 'is_discount': False, 'code': 'OM1', 'amount': '3.69'},
            {'line_num': 1, 'is_discount': False, 'code': 'OM1', 'amount': '3.69'},
            {'line_num': 2, 'is_discount': False, 'code': 'AP1', 'amount': '6.00'},
            {'line_num': 3, 'is_discount': True, 'code': 'APOM', 'amount': '-3.00'},
            {'line_num': 4, 'is_discount': False, 'code': 'AP1', 'amount': '6.00'},
            {'line_num': 5, 'is_discount': True, 'code': 'APOM', 'amount': '-3.00'},
            {'line_num': 6, 'is_discount': False, 'code': 'AP1', 'amount': '6.00'},
            {'line_num': 7, 'is_discount': True, 'code': 'APPL', 'amount': '-1.50'},
            {'line_num': 8, 'is_discount': False, 'code': 'AP1', 'amount': '6.00'},
            {'line_num': 9, 'is_discount': True, 'code': 'APPL', 'amount': '-1.50'},
            {'line_num': 10, 'total': '22.38'}
        ]
        self.assertEqual(tx_record, expected)

    def test_APPL_and_APOM_num_AP1_less_than_num_OM1(self):
        """Fails if incorrect number of APPL and APOM when AP1 and OM1 in basket."""
        basket = ['OM1', 'OM1', 'AP1', 'AP1', 'AP1', 'AP1', 'OM1', 'OM1', 'OM1']
        tx_record = views.apply_discounts(basket)
        expected = [
            {'line_num': 0, 'is_discount': False, 'code': 'OM1', 'amount': '3.69'},
            {'line_num': 1, 'is_discount': False, 'code': 'OM1', 'amount': '3.69'},
            {'line_num': 2, 'is_discount': False, 'code': 'AP1', 'amount': '6.00'},
            {'line_num': 3, 'is_discount': True, 'code': 'APOM', 'amount': '-3.00'},
            {'line_num': 4, 'is_discount': False, 'code': 'AP1', 'amount': '6.00'},
            {'line_num': 5, 'is_discount': True, 'code': 'APOM', 'amount': '-3.00'},
            {'line_num': 6, 'is_discount': False, 'code': 'AP1', 'amount': '6.00'},
            {'line_num': 7, 'is_discount': True, 'code': 'APOM', 'amount': '-3.00'},
            {'line_num': 8, 'is_discount': False, 'code': 'AP1', 'amount': '6.00'},
            {'line_num': 9, 'is_discount': True, 'code': 'APOM', 'amount': '-3.00'},
            {'line_num': 10, 'is_discount': False, 'code': 'OM1', 'amount': '3.69'},
            {'line_num': 11, 'is_discount': False, 'code': 'OM1', 'amount': '3.69'},
            {'line_num': 12, 'is_discount': False, 'code': 'OM1', 'amount': '3.69'},
            {'line_num': 13, 'total': '30.45'}
        ]
        self.assertEqual(tx_record, expected)

    # Tests of apply_discounts() using provided test data
    def test_CH1_AP1_AP1_AP1_MK1(self):
        """Fails if total does not equal 16.61."""
        basket = ['CH1', 'AP1', 'AP1', 'AP1', 'MK1']
        tx_record = views.apply_discounts(basket)
        tx_record_total = tx_record[-1]['total']
        self.assertEqual(tx_record_total, '16.61')

    def test_CH1_AP1_CF1_MK1(self):
        """Fails if total does not equal 20.34."""
        basket = ['CH1', 'AP1', 'CF1', 'MK1']
        tx_record = views.apply_discounts(basket)
        tx_record_total = tx_record[-1]['total']
        self.assertEqual(tx_record_total, '20.34')

    def test_MK1_AP1(self):
        """Fails if total does not equal 10.75."""
        basket = ['MK1', 'AP1']
        tx_record = views.apply_discounts(basket)
        tx_record_total = tx_record[-1]['total']
        self.assertEqual(tx_record_total, '10.75')

    def test_MK1_AP1(self):
        """Fails if total does not equal 11.23."""
        basket = ['CF1', 'CF1']
        tx_record = views.apply_discounts(basket)
        tx_record_total = tx_record[-1]['total']
        self.assertEqual(tx_record_total, '11.23')

    def test_AP1_AP1_CH1_AP1(self):
        """Fails if total does not equal 16.61."""
        basket = ['AP1', 'AP1', 'CH1', 'AP1']
        tx_record = views.apply_discounts(basket)
        tx_record_total = tx_record[-1]['total']
        self.assertEqual(tx_record_total, '16.61')
