from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.contrib import messages
from django.views import View

import collections
from decimal import *

from .models import Product


class CheckoutView(View):
    """Handles requests to the Checkout Page.

    GET requests return the Checkout page.

    POST requests handle Adding Product Codes, Printing Transaction, and
    starting New Transactions.
    """
    def get(self, request):
        return render(request, 'checkout/index.html')

    def post(self, request):
        # Handle Add Product Code
        if 'add_product_code' in request.POST:
            # get the product code from form input
            code = request.POST.get('product_code').upper()

            # validate and handle the user input
            if validate_product_code(code) is True:
                add_product_to_transaction(request, code)
                message = code + ' successfully added to transaction'
                messages.success(request, message)

            else:
                message = 'Invalid product code ' + code + ' submitted. Please try again.'
                messages.error(request, message)

            return HttpResponseRedirect('')  # redirect to self

        # Handle Print Transaction
        elif 'print_transaction' in request.POST:
            basket = request.session.get('tx')
            # check for items in basket
            if basket:
                request.session['tx_record'] = get_tx_record(basket)

            return render(request, 'checkout/index.html', context={'print_transaction': True})

        # Handle New Transaction
        elif 'new_transaction' in request.POST:
            messages.success(request, 'Starting a new transaction')
            request.session.flush()  # clear session data

            return HttpResponseRedirect('')  # redirect to self


# HELPER FUNCTIONS
def validate_product_code(product_code):
    """ Validates a user-entered product code.

    Arguments:
    product_code -- user-entered product code.  product_code is changed to uppercase

    Returns:
    Boolean -- True if product code exists, False otherwise
    """
    return Product.objects.filter(code=product_code.upper()).exists()


def add_product_to_transaction(request, product_code):
    """Add a string to the current session variable 'tx'. Creates the session
    variable 'tx' if it doesn't exist.

    Arguments:
    request -- Request object
    product_code -- string to be added to session variable 'tx'

    Returns None
    """
    if 'tx' in request.session:
        current_tx = request.session['tx']
        current_tx.append(product_code.upper())
        request.session['tx'] = current_tx
    else:
        request.session['tx'] = [product_code.upper()]


def get_tx_record(basket):
    """Intermediate function that applies any discounts and returns the current
    transaction record and total.  Written to allow future actions to be
    performed on the transaction record

    Arguments:
    basket -- List of product codes in current session

    Returns:
    tx_record (list) -- transaction record. Each item in the list is a
    dictionary which can be a product line, discount line, or a total line
    according to the following format.  The last item in tx_record is always the
    one and only total line.
        product line or discount line (dict) -- {
            line_num (int): 0-indexed line number
            is_discount (bool): indicates if this row is a special or not
            code (str): product or discount code
            amount (str): price or discount amount
        }
        total line (dict) -- {
            line_num (int): 0-indexed line number
            total (str): sum of transaction
        }
    Note 1: List format used to preserve order and ease serialization.
    Note 2: Decimals types formatted to strings for native Django serialization
    """
    tx_record = apply_discounts(basket)

    """ STUB: Future uses of transaction record:
        1. Save transaction record to database
        2. Serialize to JSON
    """

    return tx_record


def apply_discounts(basket):
    """Returns items in current transaction record and total with any
     applied discounts.

    Configure discount parameters here.

    Arguments:
    basket -- List of product codes in current session

    Returns:
    tx_record (list) -- transaction record. Each item in the list is a
    dictionary which can be a product line, discount line, or a total line
    according to the following format.  The last item in tx_record is always the
    one and only total line.
        product line or discount line (dict) -- {
            line_num (int): 0-indexed line number
            is_discount (bool): indicates if this row is a special or not
            code (str): product or discount code
            amount (str): price or discount amount
        }
        total line (dict) -- {
            line_num (int): 0-indexed line number
            total (str): sum of transaction
        }
    Note 1: List format used to preserve order and ease serialization.
    Note 2: Decimals types formatted to strings for native Django serialization
    """

    # Configurable Discount Parameters
    bogo_str = 'BOGO'                 # display string on transaction record
    bogo_discount_pct = Decimal(1)    # percent discount for BOGO
    appl_str = 'APPL'                 # display string on transaction record
    appl_qty = 3                      # quantity of AP1 for APPL special
    appl_discount = Decimal(1.5)      # APPL discount amount
    chmk_str = 'CHMK'                 # display string on transaction record
    chmk_discount_pct = Decimal(1)    # percent discount for CHMK
    apom_str = 'APOM'                 # display string on transaction record
    apom_discount_pct = Decimal(0.5)  # percent discount for APOM

    if not basket:
        return None

    # Initialize
    tx_record = []
    total = Decimal(0)
    cents = Decimal('0.01')  # used to quantize Decimal values to cents
    product_counts = collections.Counter(basket)
    # Grab the prices from the database once, persist for duration of transaction
    products = Product.objects.all()
    prices = {p.code: p.price for p in products}

    # Determine which specials apply
    # APPL: discount depends on quantity of apples in basket
    appl = True if product_counts['AP1'] >= appl_qty else False  # boolean if APPL applies

    # APOM: discount % off. Valid up to and including quantity of OM1
    apom_qty = product_counts['OM1']  # qty of AP1 qualifying for APOM
    apom_discount = (apom_discount_pct * prices['AP1']).quantize(cents, ROUND_HALF_UP)

    # CHMK: first milk is free if CH1 in basket, limit 1 free milk
    chmk = 'CH1' in product_counts  # boolean if CHMK applies
    chmk_discount = (chmk_discount_pct * prices['MK1']).quantize(cents, ROUND_HALF_UP)

    # BOGO: every second coffee is free, unlimited
    bogo = False  # boolean if BOGO applies
    bogo_discount = (bogo_discount_pct * prices['CF1']).quantize(cents, ROUND_HALF_UP)

    # Process each item in the basket
    line_num = 0  # can't use enumerate() as lines are inserted arbitrarily
    for item in basket:
        line = {
            'line_num': line_num,
            'is_discount': False,
            'code': item,
            'amount': '{:.2f}'.format(prices[item])
        }
        total += prices[item]
        tx_record.append(line)
        line_num += 1

        # Apply specials
        special_applied = False  # indicate if a discount line item should be added

        # Does BOGO apply?
        if item == 'CF1':
            if bogo:
                discount_str = bogo_str
                discount_amt = -bogo_discount
                bogo = False
                special_applied = True
            else:
                bogo = True

        # Does CHMK apply?
        elif chmk and item == 'MK1':
            discount_str = chmk_str
            discount_amt = -chmk_discount
            chmk = False
            special_applied = True

        # Does APPL or APOM apply?
        elif item == 'AP1':
            # Do both APPL and APOM apply?  If so, apply the greater discount
            # TODO: which special applies if discounts are equal?
            if appl and apom_qty > 0:
                if appl_discount > apom_discount:
                    discount_str = appl_str
                    discount_amt = -appl_discount
                else:
                    discount_str = apom_str
                    discount_amt = -apom_discount
                    apom_qty -= 1
                special_applied = True
            elif appl:
                discount_str = appl_str
                discount_amt = -appl_discount
                special_applied = True
            elif apom_qty > 0:
                discount_str = apom_str
                discount_amt = -apom_discount
                apom_qty -= 1
                special_applied = True

        if special_applied:
            line = {
                'line_num': line_num,
                'is_discount': True,
                'code': discount_str,
                'amount': '{:.2f}'.format(discount_amt)
            }
            total += discount_amt
            tx_record.append(line)
            line_num += 1

    # last line_item contains the sum of the current items and discounts
    tx_record.append({
        'line_num': line_num,
        'total': '{:.2f}'.format((total.quantize(cents, ROUND_HALF_UP)))
    })

    return tx_record
