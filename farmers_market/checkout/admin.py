"""Customize Django Admin Screens."""
from django.contrib import admin
from .models import Product


class ProductAdmin(admin.ModelAdmin):
    """Custom admin screen for Product model."""
    # Display fields in Model list view
    list_display = ['pk', 'code', 'name', 'price']
    list_display_links = ['code', 'name']

    # Display fields in Model edit view
    fields = ['code', 'name', 'price']

admin.site.register(Product, ProductAdmin)
