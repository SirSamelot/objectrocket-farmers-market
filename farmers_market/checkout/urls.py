from django.conf.urls import url

from checkout.views import CheckoutView

app_name = 'checkout'

urlpatterns = [
    url(r'^$', CheckoutView.as_view(), name='index')
]
