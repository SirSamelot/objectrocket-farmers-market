from .settings import *

# Allow all external hosts when running as a Docker container
# Probably not a good idea in production
ALLOWED_HOSTS = ['*']
